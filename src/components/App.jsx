import React, { Component } from 'react'

import GameBoard from './GameBoard'

export default class App extends Component {
  render() {
    return (
      <div className="App" style={appStyles}>
        <GameBoard />
      </div>
    )
  }
}

const appStyles = {
  textAlign: 'center',
  height: '100%',
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
}