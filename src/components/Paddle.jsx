import React, { Component } from 'react'


export default class Paddle extends Component {
  render() {
    const { side } = this.props

    return (
      <div className="paddle-container" style={paddleContainerStyles}>
        <div id="paddle" style={side === 'upper' ? upperStyle : lowerStyle}></div>
      </div>
    )
  }
}

const paddleContainerStyles = {
  width: '100%',
  display: 'flex',
  flexDirection: 'row',
}

const generalPaddleStyles = {
  backgroundColor: '#bbb',
  height: 20,
  width: 100,
  position: 'relative',
  left: 100,
}

const upperStyle = {
  ...generalPaddleStyles,
}

const lowerStyle = {
  ...generalPaddleStyles,
}