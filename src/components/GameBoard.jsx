import React, { Component } from 'react'

import Ball from './Ball'
import Paddle from './Paddle'


export default class GameBoard extends Component {
  render() {
    return (
      <div>
        <Paddle side="upper" />
        <div id="game-board" style={gameboardStyles}>
          <Ball />
        </div>
        <Paddle side="lower" />
      </div>
    )
  }
}

const gameboardStyles = {
  height: 600,
  width: 400,
  borderLeft: 'solid 3px #bbb',
  borderRight: 'solid 3px #bbb',
}