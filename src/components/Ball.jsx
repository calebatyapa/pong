import React, { Component } from 'react'

import logo from '../assets/logo.svg'


export default class Ball extends Component {
  render() {
    return (
      <div id="ball" style={ballContainerStyle}>
        <img style={ballStyle} src={logo} alt="ball" />
      </div>
    )
  }
}

const ballStyle = {
  animation: 'ball-spin infinite 20s linear',
}

const ballContainerStyle = {
  top: 100,
  left: 100,
  position: 'relative',
  marginTop: -3,
  marginLeft: -10,
  height: 50,
  width: 50,
}